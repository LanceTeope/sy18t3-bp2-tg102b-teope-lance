#include <iostream>
#include <string>
#include <time.h>
#include <conio.h>

using namespace std;

void Bet1(int& money, int& bet)
{
	cout << "You have " << money << " gold" << endl;
	do
	{	
		cout << "Input your bet: " << endl;
		cin >> bet;
		
		if (bet > money || bet <= 0) cout << "You don't have enough money" << endl;
	} while (bet > money || bet <= 0);
		money -= bet;
		cout << "You have " << money << " gold left" << endl;
}

int Bet2(int money, int bet)
{
	do
	{
		money -= bet;
		cout << "You have " << money << " gold left" << endl;

	} while (bet > money && bet == 0);
	money -= bet;
	cout << "You have " << money << " gold left" << endl;
	return money;
	return bet;
}

void RollDice(int& user)
{

	int dice1 = rand() % 6 + 1;
	int dice2 = rand() % 6 + 1;
	if (dice1 + dice2 == 2)
	{
		user = 13;
	}
	else 
	{
		user = dice1 + dice2;
	}
	
}

void PayOut(int bot, int player, int bet, int &money)
{
	cout << "Your Roll: " << player <<  endl; 
	cout << "Bot Roll: " << bot << endl;
	if (player == 13 && player > bot)
	{
		money += bet * 4;
		cout << "\nSNAKE EYES! You won and extra " << bet << " gold!\n" << endl;
	}
	else if (player > bot)
	{
		money += bet * 2;
		cout << "\nCongratulations, you hustler! You won " << bet << " gold!\n"<< endl;
	}
	else if (player == bot)
	{
		money += bet;
		cout << "\nDRAW! You get your money back.\n" << endl;;
	}
	else
	{
		cout << "\nYou lost some moola! :-(\n" << endl;
	}
}

void PlayRound(int bot, int player, int bet, int &money)
{
	while (money > 0)
	{
		Bet1(money, bet);
		RollDice(bot);
		RollDice(player);
		PayOut(bot, player, bet, money);
	}
	cout << "\nGAME OVER\n";
}

int main()
{
	int gold = 1000;
	int bet= 0;
	int bot = 0;
	int player = 0;
	srand(time(NULL));
	
	PlayRound(bot, player, bet, gold);
	
	_getch();
}