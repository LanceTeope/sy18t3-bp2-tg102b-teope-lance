#include<iostream>
#include<string>
#include<conio.h>
#include<time.h>

using namespace std;

struct Item
{
	string name;
	int gold;
};

Item* createItem()
{
	Item* createdItem = new Item;
	int random = rand () %5 + 1;

	if (random == 1)
	{
		createdItem->name = "Mithril Ore";
		createdItem->gold = 100;
	}
	else if (random == 2)
	{
		createdItem->name = "Sharp Talon";
		createdItem->gold = 50;
	}
	else if (random == 3)
	{
		createdItem->name = "Thick Leather";
		createdItem->gold = 25;
	}
	else if (random == 4)
	{
		createdItem->name = "Jellopy";
		createdItem->gold = 5;
	}
	else if (random == 5)
	{
		createdItem->name = "Cursed Stone";
		createdItem->gold = 0;
	}

	return createdItem;
}

void EnterDungeon(int & gold)
{
	while (gold >= 25) {
		char answer;
		int dungeonGold = 0;
		int multiplier = 1;
		int counter = 0;
		string nameItem;
		cout << "Your Current Gold is " << gold << endl
			<< "Do you want to enter the Dungeon? (Y/N)" << endl;
		do {
			cin >> answer;
			system("CLS");
		} while ((answer != 'y' && answer != 'Y') && (answer != 'n' && answer != 'N'));
		if (answer == 'n' || answer == 'N') break;
		if (counter == 0)
		{
			gold -= 25;
		}
		createItem();
		while (nameItem != "Cursed Stone")
		{
			if (counter >= 1) do cin >> answer; while ((answer != 'y' && answer != 'Y') && (answer != 'n' && answer != 'N'));
			if (answer == 'n' || answer == 'N')
			{
				gold += dungeonGold;
				cout << "You came out of the dungeon." << endl;
				break;
			}
			cout << "You collected " << nameItem << endl;
			nameItem = createdItem.name;
			cout << "The item is worth " << createdItem.gold << " gold" << endl << endl;
			if (createdItem.gold == 0)
			{
				dungeonGold *= 0;
				break;
			}
			else
			{
				dungeonGold += createdItem.gold * multiplier;
				multiplier++;
				cout << "Your total looted gold is " << dungeonGold << endl;
				cout << "Do you want to keep exploring? (Y/N)" << endl;
			}
			counter++;
		}
		counter = 0;
	}
}

int main()
{
	srand(time(NULL));
	int gold = 50;
	string item;
	EnterDungeon(gold);
	_getch();
}