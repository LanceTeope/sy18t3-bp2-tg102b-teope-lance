#include <iostream>
#include <string>
#include <conio.h>
#include <time.h>
#include <vector>

using namespace std;
//Betting system
void Bet(int& mm, int& mmBet, int& cash, int round)
{
	cout << "Rounds: " << round << "/12" << endl;
	cout << "Your money: " << cash << endl;
	cout << "Distance Left (mm): " << mm << endl << endl;
	do
	{
		cout << "Input your wager (mm): " << endl;
		cin >> mmBet;
		cout << endl;
		if (mmBet > mm || mmBet <= 0) cout << "You don't have that much mm to bet, Kaiji." << endl;
	} while (mmBet > mm || mmBet <= 0);
	system("CLS");
}
//Endings
void Endings(int cash, int distance, int round)
{
	if (cash > 20000000 && distance > 0)
	{
		cout << "YOU WIN 20 MILLION YEN AND STILL HAVE YOUR SENSE OF HEARING." << endl;
	}
	else if (cash < 20000000 && distance > 0)
	{
		cout << "YOU DID NOT WIN THE 20 MILLION YEN. Your opponent laughs at your despair. " << endl;
	}
	else if (distance <= 0)
	{
		cout << "YOU LOST YOUR SENSE OF HEARING! Your friend's are disappointed by you." << endl;
	}
}

int main()
{
	srand(time(NULL));
	int cash = 0;
	int index, bet, bot, multiplier;
	int distance = 30;
	int round = 1;
	string cardName, enemyCard;
	bool isWinning = false;

	vector<string> deck1;
	vector<string> deck2;
	// Emperor cards
	vector <string> EmperorVec;
	EmperorVec.push_back("Emperor");
	EmperorVec.push_back("Civilian");
	EmperorVec.push_back("Civilian");
	EmperorVec.push_back("Civilian");
	EmperorVec.push_back("Civilian");
	//Slave Cards
	vector <string> SlaveVec;
	SlaveVec.push_back("Slave");
	SlaveVec.push_back("Civilian");
	SlaveVec.push_back("Civilian");
	SlaveVec.push_back("Civilian");
	SlaveVec.push_back("Civilian");

	while (round < 13 && distance > 0) //continues to loop as long as you have mm or rounds haven't ended
	{
		//switching Sides
		if (round % 6 < 4 && round > 0)
		{
			deck1 = EmperorVec;
			deck2 = SlaveVec;
			multiplier = 1;
			cout << "Kaiji's deck: Emeperor" << endl;
		}
		else
		{
			deck1 = SlaveVec;
			deck2 = EmperorVec;
			multiplier = 5;
			cout << "Kaiji's deck: Slave" << endl;
		}
		Bet(distance, bet, cash, round);

		while (isWinning == false) {
			for (unsigned int i = 0; i < deck1.size(); i++)
			{
				cout << "[" << i << "]" << "-" << deck1[i] << endl;
			}
			// Ask what card to pick. This is by index
			do
			{
				cout << "Select your Card: ";
				cin >> index;
				cout << endl;
				if (index > deck1.size()) cout << "You don't have that amount of cards, Kaiji." << endl;
			} while (index > deck1.size());
			// Make enemy randomizer
			cardName = deck1[index];
			int random = rand() % deck2.size();
			enemyCard = deck2[random];
			//win round conditions
			if ((cardName == "Emperor" && enemyCard == "Slave") || (cardName == "Slave" && enemyCard == "Civilian") || (cardName == "Civilian" && enemyCard == "Emperor"))
			{
				cout << "YOU LOST! Your " << cardName << " was defeated by a " << enemyCard << " card." << endl;
				distance -= bet;
				isWinning = true;
				_getch();
				system("CLS");
			}
			else if (cardName == "Civilian" && enemyCard == "Civilian")
			{
				cout << "ITS A DRAW! You both drew a Civilian Card. You haven't won the round yet." << endl;
				deck1.erase(deck1.begin() + index);
				deck2.erase(deck2.begin() + random);
				_getch();
				system("CLS");
			}
			else
			{
				cout << "YOU WON! Your " << cardName << " defeated " << enemyCard << " card!" << endl;
				cash = cash + (bet * 100000 * multiplier);
				cout << "You have a total of " << cash << " yen." << endl;
				isWinning = true;
				_getch();
				system("CLS");
			}
		}
		round++;
		isWinning = false;
	}
	Endings(cash, distance, round);
	_getch();
}
