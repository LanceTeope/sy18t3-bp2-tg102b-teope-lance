#include <iostream>
#include <string>
#include <cstdlib>
#include <conio.h>
#include "Monsters.h"

using namespace std;

Monster::Monster(string _MonsterName, int _MonsterHP, int _MonsterMaxHP, int _MonsterPOW, int _MonsterVIT, int _MonsterAGI, int _MonsterDEX, int _Monsterarmor)
{
	this->MonsterName = _MonsterName;
	this->MonsterHP = _MonsterHP;
	this->MonsterMaxHP = _MonsterMaxHP;
	this->MonsterPOW = _MonsterPOW;
	this->MonsterVIT = _MonsterVIT;
	this->MonsterAGI = _MonsterAGI;
	this->MonsterDEX = _MonsterDEX;
	this->Monsterarmor = _Monsterarmor;
}

void Monster::MonsterDisplayStats()
{
	cout << "Monster Type: " << this->MonsterName << endl;
	cout << "Monster HP: " << this->MonsterHP << endl;
	cout << "Monster MaxHP: " << this->MonsterMaxHP << endl;
	cout << "Monster POW: " << this->MonsterPOW << endl;
	cout << "Monster VIT: " << this->MonsterVIT << endl;
	cout << "Monster AGI: " << this->MonsterAGI << endl;
	cout << "MonsterDEX: " << this->MonsterDEX << endl;
}