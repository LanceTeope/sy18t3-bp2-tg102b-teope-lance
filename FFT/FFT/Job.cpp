#include <iostream>
#include <string>
#include <cstdlib>
#include <conio.h>
#include "Player.h"
#include "Job.h"

using namespace std;

Job::Job(string _jobName, string _jobWeapon, string _jobArmor, int _HP, int _MaxHP, int _POW, int _VIT, int _AGI, int _DEX, int _startingMoney, int _jobweapon, int _jobarmor)
{
	this->jobName = _jobName;
	this->jobWeapon = _jobWeapon;
	this->jobArmor = _jobArmor;
	this->HP = _HP;
	this->MaxHP = _MaxHP;
	this->POW = _POW;
	this->VIT = _VIT;
	this->AGI = _AGI;
	this->DEX = _DEX;
	this->startingMoney = _startingMoney;
	this->jobweapon = _jobweapon;
	this->jobarmor = _jobarmor;
}