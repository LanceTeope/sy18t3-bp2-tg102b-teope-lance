#include "Map.h"
#include "Player.h"
#include "Monsters.h"
#include "Type.h"
#include "Shop.h"
#include "Weapons.h"
#include "Armor.h"
#include <iostream>
#include <string>
#include <cstdlib>
#include <conio.h>

using namespace std;

Map::Map(Player *player, Type *type)
{
}

void Map::Chance(int _chance)
{
	this->chance = _chance = rand() % 100 + 1;
}

void Player::Move(Player * player, Shop * shop, Armor * armor, Weapons * weapon)
{
	cout << "Press [W] for North" << endl;
	cout << "Press [S] for South" << endl;
	cout << "Press [D] for East" << endl;
	cout << "Press [A] for West" << endl;
	cin >> this->inputDirection;

	if (inputDirection == 'w' || inputDirection == 'W')
	{
		this->y++;
	}
	else if (inputDirection == 's' || inputDirection == 'S')
	{
		this->y--;
	}
	else if (inputDirection == 'd' || inputDirection == 'D')
	{
		this->x++;
	}
	else if (inputDirection == 'a' || inputDirection == 'A')
	{
		this->x--;
	}
	else
	{
		cout << "INVALID INPUT" << endl;
	}
	cout << "Your position: (" << x << "," << y << ")" << endl;
	_getch();
	system("cls");
	if (this->x == 1 && this->y == 1)
	{
		cout << "A Secret Merchant has appeared!" << endl;
		_getch();
		system("CLS");
		shop->Entershop(player, weapon, armor);
	}
	
}

void Map::Spawn(Player *player, Monster * monster, Type * type)
{
	Chance(this->chance);
	if (chance <= 20)
	{
		cout << "There are no monsters in the area" << endl;
	}
	if (chance > 20 && chance <= 45)
	{
		type = new Type ("Goblin", 15, 15, 30, 5, 9, 16, 4);
		cout << "A weak Goblin has appeared!" << endl;
	}
	else if (chance > 45 && chance <= 70)
	{
		type = new Type("Ogre", 25, 25, 35, 7, 10, 6, 6);
		cout << "An avearage Ogre has appeared!" << endl;
	}
	else if (chance > 70 && chance <= 95)
	{
		type = new Type("Orc", 40, 40, 28, 16, 4, 4, 8);
		cout << "A hardened Orc has appeared!" << endl;
	}
	else if (chance > 95)
	{
		type = new Type("Orc Lord", 70, 70, 50, 26, 4, 2, 10);
		cout << "The Orc Lord approaches! Be ready, hero!" << endl;
	}
	monster->MonsterName =type->typeName;
	monster->MonsterHP = type->TypeHP;
	monster->MonsterMaxHP = type->TypeMaxHP;
	monster->MonsterPOW = type->TypePOW;
	monster->MonsterVIT = type->TypeVIT;
	monster->MonsterAGI = type->TypeAGI;
	monster->MonsterDEX = type->TypeDEX;
	monster->Monsterarmor = type->Typearmor;
	_getch();
	system("cls");

	if (chance > 20)
	{
		Combat(player, monster);
	}
}

void Map::Combat(Player * player, Monster * monster)
{
	player->pDamage = (player->pPOW + player->weapon) - (monster->MonsterVIT + monster->Monsterarmor);
	player->pHitrate = (player->pDEX / monster->MonsterAGI) * 100;
	monster->Monsterdamage = (monster->MonsterPOW) - (monster->MonsterVIT + player->armor);
	monster->MonsterHitrate = (monster->MonsterDEX / player->pAGI) * 100;
	while (player->pHP > 0 && monster->MonsterHP > 0)
	{
		if (monster->MonsterName == "Goblin")
		{
			cout << "You are in combat with a " << monster->MonsterName << "." << endl << endl;
		}
		else
		{
			cout << "You are in combat with an " << monster->MonsterName << "." << endl << endl;
		}

		monster->MonsterDisplayStats();
		cout << "====================================" << endl;
		player->DisplayCombatStats();
		cout << "====================================" << endl;
		cout << "[1] Attack" << endl;
		cout << "[2] Run" << endl;
		cin >> player->combatOption;
		system("cls");
		
		if (player->combatOption == 1)
		{
			Chance(this->chance);
			cout << "You prepare for the attack." << endl;
			_getch();
			if (player->pHitrate < 20)
			{
				player->pHitrate = 20;
			}
			else if (player->pHitrate > 80)
			{
				player->pHitrate = 80;
			}

			if (player->pHitrate <= this->chance)
			{
				cout << "You did " << player->pDamage << " damage to " << monster->MonsterName << endl;
				monster->MonsterHP -= player->pDamage;
			}
			else if (player->pHitrate > this->chance)
			{
				cout << "You missed the " << monster->MonsterName << endl;
			}

			_getch();
			system("cls");
		}
		else if (player->combatOption == 2)
		{
			cout << "You are trying to run away." << endl << endl;
			_getch();
			Chance(this->chance);
			if (chance <= 25)
			{
				cout << "You have successfully fled the battle!" << endl;
				_getch();
				system("cls");
				break;
			}
			else if (chance > 25)
			{
				cout << "You failed to runaway." << endl;
				_getch();
				system("cls");
			}
		}
		
		if (monster->MonsterHP > 0)
		{
			Chance(this->chance);
			cout << "The " << monster->MonsterName << " is preparing to attack." << endl;
			_getch();
			if (monster->MonsterHitrate < 20)
			{
				monster->MonsterHitrate = 20;
			}
			else if (monster->MonsterHitrate > 80)
			{
				monster->MonsterHitrate = 80;
			}

			if (monster->MonsterHitrate <= this->chance)
			{
				cout << "The " << monster->MonsterName << " did " << monster->Monsterdamage << " damage to you." << endl;
				player->pHP -= monster->Monsterdamage;
			}
			else if (monster->MonsterHitrate > this->chance)
			{
				cout << "The " << monster->MonsterName << " swung and missed." << endl;
			}
			_getch();
			system("CLS");
		}
	};
	//Reward
	if (monster->MonsterName == "Goblin" && monster->MonsterHP <= 0)
	{
		cout << "YOU DEAFEATED THE GOBLIN!" << endl;
		cout << "====================================" << endl;
		cout << "You have earned 100 exp and 10 gold!" << endl;
		player->exp += 100;
		player->money += 10;
		_getch();
		system("cls");
	}
	else if (monster->MonsterName == "Ogre" && monster->MonsterHP <= 0)
	{
		cout << "YOU DEAFEATED THE OGRE!" << endl;
		cout << "You have earned 250 exp and 50 gold!" << endl;
		player->exp += 250;
		player->money += 50;
		_getch();
		system("cls");
	}
	else if (monster->MonsterName == "Orc" && monster->MonsterHP <= 0)
	{
		cout << "YOU DEAFEATED THE ORC!" << endl;
		cout << "You have earned 500 exp and 100 gold!" << endl;
		player->exp += 500;
		player->money += 100;
		_getch();
		system("cls");
	}
	else if (monster->MonsterName == "Orc Lord" && monster->MonsterHP <= 0)
	{
		cout << "YOU DEAFEATED THE ORC LORD! ARE YOU A GOD?!" << endl;
		cout << "You have earned 1000 exp and 1000 gold!" << endl;
		player->exp += 1000;
		player->money += 1000;
		_getch();
		system("cls");
	}
	//Level Up
	if (player->exp >= player->requiredExp)
	{
		player->LevelUp(player);
	}


}