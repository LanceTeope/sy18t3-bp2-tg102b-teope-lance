#pragma once
#include <iostream>
#include <string>
#include <cstdlib>

using namespace std;
class Player;
class Armor
{
public:
	string armorName;
	int armorDefense, armorCost;

	Armor(string _armorName, int _armorDefense, int _armorCost);
};
