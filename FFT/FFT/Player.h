#pragma once
#include <iostream>
#include <string>
#include <cstdlib>

using namespace std;
class Job;
class Map;
class Monster;
class Shop;
class Weapons;
class Armor;
class Player
{
public:
	string playerName;
	Job* job;
	string className;
	string weaponEquipped;
	string armorEquipped;
	int x, y;
	char inputDirection;
	int pHP, pMaxHP, pPOW, pVIT, pAGI, pDEX;
	int level=1;
	int exp = 0;
	int pHitrate;
	int pDamage;
	int armor;
	int weapon;
	int money;
	int requiredExp = level * 1000;
	int classOption, shopOption, option, combatOption;
	

	Player(string _playerName, string _weaponEquipped, string _armorEquipped, int _pHP, int _pMaxHP, int _pPOW, int _pVIT, int _pAGI, int _pDEX, int _money, int _weapon, int _armor);

	void pickClass();
	void Rest();
	void DisplayStats();
	void DisplayCombatStats();
	void Move(Player * player, Shop * shop, Armor * armor, Weapons * weapon);
	void LevelUp(Player * player);
};