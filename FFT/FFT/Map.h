#pragma once
#include <iostream>
#include <string>
#include <cstdlib>
using namespace std;
class Player;
class Monster;
class Type;

class Map
{
public:
	int chance;
	int runChance;

	Monster *monster;
	Map(Player * player, Type *type);

	void Chance(int _chance);
	void Spawn(Player * player, Monster * monster, Type * type);
	void Combat(Player * player, Monster * monster);
};

