#pragma once
#include <iostream>
#include <string>
#include <cstdlib>

using namespace std;

class Player;
class Job
{
public:
	string jobName;
	string jobWeapon;
	string jobArmor;
	int HP, MaxHP, POW, VIT, AGI, DEX, startingMoney, jobweapon, jobarmor;

	Job(string _jobName, string _jobWeapon, string _jobArmor, int _HP, int _MaxHP, int _POW, int _VIT, int _AGI, int _DEX, int _startingMoney, int _jobweapon, int _jobarmor);

};

