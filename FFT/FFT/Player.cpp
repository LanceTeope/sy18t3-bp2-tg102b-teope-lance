#include <iostream>
#include <string>
#include <cstdlib>
#include <conio.h>
#include "Player.h"
#include "Job.h"
#include "Map.h"

using namespace std;

Player::Player(string _playerName, string _weaponEquipped, string _armorEquipped, int _pHP, int _pMaxHP, int _pPOW, int _pVIT, int _pAGI, int _pDEX, int _money, int _weapon, int _armor)
{
	this->playerName = _playerName;
	this->weaponEquipped = _weaponEquipped;
	this->armorEquipped = _armorEquipped;
	this->pHP = _pHP;
	this->pMaxHP = _pMaxHP;
	this->pPOW = _pPOW;
	this->pVIT = _pVIT;
	this->pAGI = _pAGI;
	this->pDEX = _pDEX;
	this->money = _money;
	this->weapon = _weapon;
	this->armor = _armor;
}

void Player::pickClass()
{
	cout << "Press [1] for Warrior " << endl;
	cout << "Press [2] for Thief" << endl;
	cout << "Press [3] for Crusader" <<endl;
	cout << "Select you class: ";
	cin >> this->classOption;
	if (this->classOption == 1)
	{
		this->job = new Job("Warrior","Broad Sword", "You are naked", 100, 100, 55, 16, 24, 30, 0, 20, 0);
		cout << endl << "You selected Warrior. You will have great strength and start with a better weapon." << endl << endl;
	}
	else if (this->classOption == 2)
	{
		this->job = new Job("Thief", "Bare hands", "Stolen gloves",  90, 90, 40, 20, 40, 45, 50, 0, 0);
		cout << endl << "You selected Thief. You are agile and nimble at battle." << endl << endl;
	}
	else if (this->classOption == 3)
	{
		this->job = new Job("Crusader", "Rock", "Man Bra",  130, 130, 50, 35, 30, 24, 0, 2, 1);
		cout << endl << "You selected Crusader. Your might makes you take tough blows." << endl << endl;
	}
	this->className = this->job->jobName;
	this->pHP = this->job->HP;
	this->pMaxHP = this->job->MaxHP;
	this->pPOW = this->job->POW;
	this->pVIT = this->job->VIT;
	this->pAGI = this->job->AGI;
	this->pDEX = this->job->DEX;
	this->money = this->job->startingMoney;
	this->armorEquipped = this->job->jobArmor;
	this->armor = this->job->jobarmor;
	this->weaponEquipped = this->job->jobWeapon;
	this->weapon = this->job->jobweapon;
	system("pause");
	system("cls");
}


void Player::Rest()
{
	cout << "You got twice the amount of sleep as a programmer!" << endl;
	this->pHP = this->pMaxHP;
	cout << "Your current HP has been restored" << endl;
	_getch();
	
}

void Player::DisplayStats()
{
	cout << "Class: " << this->className << endl;
	cout << "Level: " << this->level << endl;
	cout << "HP: " << this->pHP << "/" << this->pMaxHP << endl;
	cout << "Experience: " << this->exp << "/" << this->requiredExp << endl;
	cout << "Gold: " << this->money << endl;
	cout << "====================================" << endl;
	cout << "POW: " << this->pPOW << endl;
	cout << "VIT: " << this->pVIT << endl;
	cout << "AGI: " << this->pAGI << endl;
	cout << "DEX: " << this->pDEX << endl;
	cout << "====================================" << endl;
	cout << "Armor Equipped: " << this->armorEquipped << endl;
	cout << "Armor Defense : " << this->armor << endl << endl;;
	cout << "Weapon Equipped: " << this->weaponEquipped << endl;
	cout << "Weapon Damage: " << this->weapon << endl;
	cout << "====================================" << endl;
	cout << "Your Current Position: (" << this->x << "," << this->y << ")" << endl;

}

void Player::DisplayCombatStats()
{
	cout << "Your HP: " << this->pHP << "/" << this->pMaxHP << endl;
	cout << "Your POW: " << this->pPOW << endl;
	cout << "Your VIT: " << this->pVIT << endl;
	cout << "Your AGI: " << this->pAGI << endl;
	cout << "Your DEX: " << this->pDEX << endl;
}

void Player::LevelUp(Player* player)
{
	player->exp %= player->requiredExp;
	player->level++;
	cout << "You have leveled up!" << endl;
	cout << "Your current level is " << player->level << "." << endl << endl;
	player->pMaxHP += rand() % 40;
	player->pPOW += rand() % 6;
	player->pVIT += rand() % 6;
	player->pAGI += rand() % 6;
	player->pDEX += rand() % 6;
	player->pHP = player->pMaxHP;
	cout << "Your stats have increased!" << endl;
	cout << "====================================" << endl;
	player->DisplayCombatStats();
	_getch();
	system("cls");
}