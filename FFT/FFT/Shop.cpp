#include "Shop.h"
#include <iostream>
#include <string>
#include <cstdlib>
#include <conio.h>
#include "Armor.h"
#include "Weapons.h"
#include "Player.h"

using namespace std;

Shop::Shop(Player *player, Weapons * weapon, Armor * armor)
{
}

void Shop::Entershop(Player * player, Weapons * weapon, Armor * armor)
{
	while (player->option != 3)
	{
		cout << "Welcome to the Shop!" << endl;
		cout << "Current Gold: " << player->money << endl << endl;
		cout << "What would you like to buy? " << endl;
		cout << "====================================" << endl;
		cout << "[1] Buy Weapons" << endl;
		cout << "[2] Buy Armor" << endl;
		cout << "[3] Leave Shop" << endl;
		cin >> player->option;
		system("cls");

		if (player->option == 1)
		{
			while (player->shopOption != 5)
			{

				cout << "I collected these from Google docs! Pick your poison." << endl;
				cout << "====================================" << endl;
				cout << "[1] Short Sword: 5 damage (10 gold)" << endl;
				cout << "[2] Long Sword: 10 damage (50 gold)" << endl;
				cout << "[3] Broad Sword: 20 damage (200 gold)" << endl;
				cout << "[4] Excalibur: 999 damage (9999 gold)" << endl;
				cout << "[5] Go back to shop" << endl;
				cout << "Current Gold: " << player->money << endl;
				cout << "See anything you like? ";
				cin >> player->shopOption;
				system("cls");

				if (player->shopOption == 1)
				{
					weapon = new Weapons("Short Sword", 5, 10);
				}
				else if (player->shopOption == 2)
				{
					weapon = new Weapons("Long Sword", 10, 50);
				}
				else if (player->shopOption == 3)
				{
					weapon = new Weapons("Broad Sword", 20, 200);
				}
				else if (player->shopOption == 4)
				{
					weapon = new Weapons("Excalibur", 999, 9999);
				}
				if (player->shopOption == 5)
				{
					break;
					system("cls");
				}

				if (player->money >= weapon->weaponCost)
				{
					player->money -= weapon->weaponCost;
					cout << "You bought " << weapon->weaponName << "! You have " << player->money << " gold left." << endl;
					player->weaponEquipped = weapon->weaponName;
					player->weapon = weapon->weaponDamage;
					_getch();
					system("cls");
				}
				else if (player->money < weapon->weaponCost)
				{
					cout << "You don't have enough money for this item!" << endl;
					_getch();
					system("cls");
				}
			}
		}
				if (player->option == 2)
				{
					while (player->shopOption != 4)
					{

						cout << "Armor from the finest Arial font!" << endl;
						cout << "====================================" << endl;
						cout << "[1] Leather Mail: 2 armor (50 gold)" << endl;
						cout << "[2] Chain Mail: 4 armor (100 gold)" << endl;
						cout << "[3] Plate Armor: 8 armor (300 gold)" << endl;
						cout << "[4] Go back to shop" << endl;
						cout << "Current Gold: " << player->money << endl;
						cout << "Anything fitting for the hero? ";
						cin >> player->shopOption;
						system("cls");
						if (player->shopOption == 1)
						{
							armor = new Armor("Leather Mail", 2, 50);
						}
						else if (player->shopOption == 2)
						{
							weapon = new Weapons("Chain Mail", 4, 100);
						}
						else if (player->shopOption == 3)
						{
							weapon = new Weapons("Plate Armor", 8, 300);
						}
						if (player->shopOption == 4)
						{
							break;
							system("cls");
						}

						if (player->money >= armor->armorCost)
						{
							player->money -= armor->armorCost;
							cout << "You bought " << armor->armorName << "! You have " << player->money << " gold left." << endl;
							player->armorEquipped = armor->armorName;
							player->armor = armor->armorDefense;
							_getch();
							system("cls");
						}
						else if (player->money < armor->armorCost)
						{
							cout << "You need don't have enough money for this item!" << endl;
							_getch();
							system("cls");
						}

					}
				}
	}
}


