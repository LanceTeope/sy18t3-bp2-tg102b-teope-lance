#include <iostream>
#include <string>
#include <cstdlib>
#include <conio.h>
#include "Armor.h"

using namespace std;

Armor::Armor(string _armorName, int _armorDefense, int _armorCost)
{
	this->armorName = _armorName;
	this->armorDefense = _armorDefense;
	this->armorCost = _armorCost;
}