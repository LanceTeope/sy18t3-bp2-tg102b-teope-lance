#include <iostream>
#include <string>
#include <cstdlib>
#include <conio.h>
#include "Weapons.h"

using namespace std;

Weapons::Weapons(string _weaponName, int _weaponDamage, int _weaponCost)
{
	this->weaponName = _weaponName;
	this->weaponDamage = _weaponDamage;
	this->weaponCost = _weaponCost;
}