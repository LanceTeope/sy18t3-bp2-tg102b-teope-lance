#pragma once
#include <iostream>
#include <string>
#include <cstdlib>

using namespace std;

class Stats
{
public:
	int HP;
	int POW;
	int VIT;
	int AGI;
	int DEX;
	int damage;
	int hitrate;
	Stats(int HP, int POW, int VIT, int AGI, int DEX, int damage, int hitrate);
};

