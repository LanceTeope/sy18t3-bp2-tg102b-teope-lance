#include <iostream>
#include <string>
#include <cstdlib>
#include <conio.h>
#include "Monsters.h"
#include "Type.h"

using namespace std;


Type::Type(string _typeName, int _TypeHP, int _TypeMaxHP, int _TypePOW, int _TypeVIT, int _TypeAGI, int _TypeDEX, int _Typearmor)
{
	this->typeName = _typeName;
	this->TypeHP = _TypeHP;
	this->TypeMaxHP = _TypeMaxHP;
	this->TypePOW = _TypePOW;
	this->TypeVIT = _TypeVIT;
	this->TypeAGI = _TypeAGI;
	this->TypeDEX = _TypeDEX;
	this->Typearmor = _Typearmor;
}