#pragma once
#include <iostream>
#include <string>
#include <cstdlib>

using namespace std; 
class Monster
{
public:
	string MonsterName;
	int MonsterHP, MonsterMaxHP, MonsterPOW, MonsterVIT, MonsterAGI, MonsterDEX, Monsterdamage, Monsterarmor, MonsterHitrate;

	Monster(string MonsterName, int MonsterHP, int MonsterMaxHP, int MonsterPOW, int MonsterVIT, int MonsterAGI, int MonsterDEX, int Monsterarmor);

	void MonsterDisplayStats();
};

