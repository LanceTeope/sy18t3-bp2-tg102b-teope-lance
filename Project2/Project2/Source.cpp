#include <iostream>
#include <string>
#include <time.h>
#include <conio.h>

using namespace std;

void printArray(string stuff[])
{
	for (int i = 0; i < 8; i++)
	{
		cout << stuff[i] << endl;
	}
}

int count(string stuff[], string num)
{
	int number = 0;
	for (int i = 0; i < 8; i++)
	{
		if (stuff[i] == num)
			number++;
	}
	return number;
}

int main()
{
	string items[] = { "Red Potion", "Blue Potion", "Yggdrasil Leaf", "Elixir", "Teleport Scroll", "Red Potion", "Red Potion", "Elixir" };
	string input;
	printArray(items);

	cout << "Enter an item to search for: ";
	getline(cin, input);
	int result = count(items, input);
	cout << "There are " << result << " of the item you have searched" <<  endl;


	_getch();
}