#include <iostream>
#include <string>
#include <time.h>
#include <conio.h>

using namespace std;

void Sort(int arr[])
{
	int i, j, smallest;
	cout << "Different Packages: ";
	for (i = 0; i < 7; i++)
	{
		smallest = i;
		for (j = i + 1; j < 7; j++)
			//finds smallest
			if (arr[j] < arr[smallest])
				smallest = j;
		//swap
		int temp = arr[smallest];
		arr[smallest] = arr[i];
		arr[i] = temp;
		//print
		cout << arr[i] << " ";
	}
	cout << "\n\n";
}
//Case #2 Function
int Suggest(int arr[], int amount, int money, int buy)
{
	char choice;
	for (int i = 0; i < 7; i++)
		if (amount <= arr[i])
		{
			cout << "Woops! You are lacking " << amount << " gold! Would you like to buy a package of " << arr[i] << " gold? (Y/N) \n";
			cin >> choice;
			switch (choice)
			{
			case 'Y': case 'y':
				cout << "\nYou bought the Package along with the item, YOU NERD!" << endl;
				money = (money + arr[i]) - buy;
				cout << "You have " << money << " gold\n\n";
				system("pause");
				system("CLS");
			case 'N': case 'n':
				system("CLS");
			}
			break;
		}
	return money;
}

int main()
{
	//Declarations
	char start;
	int remaining = 0, missing;
	int want, gold = 250;
	bool cond = true;
	int packages[] = { 30750, 1780, 500, 250000, 13333, 150, 4050 };
	//Ask Consumer
	while (true)
	{
		Sort(packages);
		do {
			cout << "Do you want to buy? (Y/N)" << endl;
			cin >> start;

		} while (start != 'y' && start != 'Y' && start != 'n' && start != 'N');
		if (start == 'n' || start == 'N') break;
		//Ask Item Price
		cout << "\nYou have " << gold << " gold" << endl;
		cout << "Item you want to buy: ";
		cin >> want;
		cout << "\n";
		//Case #1
		if (want <= gold)
		{
			missing = gold - want;
			cout << "Your Remaining Gold: " << missing << "\n\n";
			gold = missing;
			system("pause");
			system("CLS");
		}
		//Case #2
		else if (want >= gold)
		{
			missing = want - gold;
			gold = Suggest(packages, missing, gold, want);
			//Case #3
			if (want > packages[6] + gold)
			{
				cout << "There is no package that will satisfy your wants! PEASANT! \n\n";
			}
		}
	}
} //END//