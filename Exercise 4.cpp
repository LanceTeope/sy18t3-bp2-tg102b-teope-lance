#include <conio.h>
#include <string>
#include <time.h>
#include <iostream>

using namespace std;

struct Monster
{
	string name;
	int HP;
	int AD;
};

void Fight(Monster*& Monster1, Monster*& Monster2, int randomizer)
{
	int roll;
	while (Monster1->HP != 0 && Monster2->HP != 0)
	{
		randomizer;

		if (roll = 2)
		{
			cout << Monster1->name << " attacked " << Monster2->name << endl;
			Monster2->HP = Monster2->HP -Monster1->AD;
		}
		else if (roll = 1)
		{
			cout << Monster2->name << " attacked " << Monster1->name << endl;
			Monster1->HP = Monster1->HP -Monster2->AD;
		}
		cout << Monster1->name << " HP: " << Monster1->HP << endl;
		cout << Monster2->name << " HP: " << Monster2->HP<<endl;
		_getch();
		system("CLS");
	}
}

int main()
{
	srand(time(NULL));
	Monster *Monster1 = new Monster;
	Monster *Monster2 = new Monster;
	int randomizer = rand() % 2 +1;

	//Name Monsters
	cout << "Monster #1 name: ";
	cin >> Monster1->name;
	cout << "Monster #2 name: ";
	cin >> Monster2->name;
	system("CLS");
	//HP Monsters
	cout << "Monster #1 HP: ";
	cin >> Monster1->HP;
	cout << "Monster #2 HP: ";
	cin >> Monster2->HP;
	system("CLS");
	//AD Monsters
	cout << "Monster #1 Attack Damage: ";
	cin >> Monster1->AD;
	cout << "Monster #2 Attack Damage: ";
	cin >> Monster2->AD;
	system("CLS");
		
	cout << "The fight begins!" << endl;
	Fight(Monster1, Monster2, randomizer);
	cout << "GAME OVER" << endl;
	

}