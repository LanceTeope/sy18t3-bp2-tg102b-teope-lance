#include <iostream>
#include <string>
#include <time.h>
#include <conio.h>

using namespace std;

int printFactorial(int number)
{
	int n = number;
	int factorial = 1;
	for (int i = 1; i <= n; i++)
	{
		factorial = factorial * i;
	}
	cout << "The Factorial of " << number << " is " << factorial;
	return number;
}

int main()
{
	int yeet;
	cout << "Input a positive number: \n";
	cin >> yeet;
	printFactorial(yeet);

	_getch();
}