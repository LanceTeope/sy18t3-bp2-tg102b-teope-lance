#include<iostream>
#include<string>
#include<conio.h>
#include<time.h>

using namespace std;

struct Item
{
	string name;
	int gold;
};

Item randomItem(int random)
{
	Item lootType[5];


	lootType[0] = { "Mythril Ore", 100 };
	lootType[1] = { "Sharp Talon", 50 };
	lootType[2] = { "Thick Leather", 25 };
	lootType[3] = { "Jellopy", 5 };
	lootType[4] = { "Cursed Stone", 0 };

	return lootType[random];
}

void EnterDungeon(int & gold)
{
	while (gold >= 25) {
		char answer;
		int dungeonGold = 0;
		int multiplier = 1;
		Item loot;
		int counter = 0;
		cout << "Your Current Gold is " << gold << endl
			<< "Do you want to enter the Dungeon? (Y/N)" << endl;
		do {
			cin >> answer;
			system("CLS");
		} while ((answer != 'y' && answer != 'Y') && (answer != 'n' && answer != 'N'));
		if (answer == 'n' || answer == 'N') break;
		if (counter == 0)
		{
			gold -= 25;
		}
		while (loot.name != "Cursed Stone")
		{
			int random = rand() % 5;
			loot = randomItem(random);
			if (counter >= 1) do cin >> answer; while ((answer != 'y' && answer != 'Y') && (answer != 'n' && answer != 'N'));
			if (answer == 'n' || answer == 'N')
			{
				gold += dungeonGold;
				cout << "You came out of the dungeon." << endl;
				break;
			}
			cout << "You collected " << loot.name << endl;
			cout << "The item is worth " << loot.gold << " gold" << endl << endl;
			if (loot.gold == 0)
			{
				dungeonGold *= 0;
				break;
			}
			else
			{
				dungeonGold += loot.gold * multiplier;
				multiplier++;
				cout << "Your total looted gold is " << dungeonGold << endl;
				cout << "Do you want to keep exploring? (Y/N)" << endl;
			}
			counter++;
		}
		counter = 0;
	}
}

int main()
{
	srand(time(NULL));
	int gold = 50;
	string item;
	EnterDungeon(gold);
	_getch();
}