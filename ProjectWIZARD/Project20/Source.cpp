#include <iostream>
#include <string>
#include <cstdlib>
#include <conio.h>
#include <time.h>

#include "Spell.h"
#include "Wizard.h"

using namespace std;

void attkSpell(Wizard *user, Spell *spell)
{
	spell->callSpell(user);
	spell->damage = rand() % 20 + 10;
	user->applyDamage(spell);
}

int main()
{
	system("color 2");
	srand(time(NULL));
	Wizard *wizard1 = new Wizard;
	wizard1->hp = 100;
	wizard1->mp = 100;

	Wizard *wizard2 = new Wizard;
	wizard2->hp = 100;
	wizard2->mp = 100;

	Spell *spell = new Spell;
	spell->cost = 10;
	
	bool isAlive = true;
	bool isSpellUsable = true;
	cout << "THE BATTLE BEGINS!" << endl;
	_getch();
	while (isAlive == true && isSpellUsable == true) {
		
		cout << "Wizard 1 HP: " << wizard1->hp << endl;
		cout << "Wizard 1 MP: " << wizard1->mp << endl;

		cout << "Wizard 2 HP: " << wizard2->hp << endl;
		cout << "Wizard 2 MP: " << wizard2->mp << endl;

		attkSpell(wizard1, spell);
		attkSpell(wizard2, spell);
		
		_getch();
		system("CLS");
		
		
		if (wizard1->hp <= 0 && wizard2->hp > 0)
		{
			cout << "Wizard 2 has won the battle!" << endl;
			isAlive = false;
		}
		else if (wizard2->hp <= 0 && wizard1->hp > 0)
		{
			cout << "Wizard 1 has won the battle!" << endl;
			isAlive = false;
		}
		else if (wizard1->hp <= 0 && wizard2->hp <= 0)
		{
			cout << "It is a Draw!"<< endl;
			isAlive = false;
		}
			
		if (wizard1->mp <= 0 && wizard2->mp > 0)
		{
			cout << "Wizard 2 has won the battle!" << endl;
			isAlive = false;
		}
		else if (wizard2->mp <= 0 && wizard1->mp > 0)
		{
			cout << "Wizard 1 has won the battle!" << endl;
			isAlive = false;
		}
		else if (wizard1->mp < 10 && wizard2->hp < 10)
		{
			cout << "It is a Draw!" << endl;
			isSpellUsable = false;
		}

		
	}
	cout << "GAME OVER" << endl;
	 


	_getch();
}