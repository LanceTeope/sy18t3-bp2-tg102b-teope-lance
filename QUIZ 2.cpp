#include <iostream>
#include <string>
#include <time.h>
#include <conio.h>

using namespace std;

//Node
struct node
{
	string name;
	node* next, *prev;
};

int LordCommander(int random)
{
	return rand() % random;
}

//Function that passes cloak between soldiers
 int PassingCloak(node*& nodes, int& size, int start)
{
	 int roll = LordCommander(size) + 1;

	 int address = (start + roll ) % size;
	 cout << (nodes + address)->name << " gets kicked." << endl << endl;
	
	 for (int i = address; i < size - 1; i++)
	 {
		 swap(*(nodes + i), *(nodes + (i + 1)));
	 }
	 size--;

	 return address;
}

int main()
{
	srand(time(NULL));
	cout << "Lord Commander is choosing who will get the cloak" << endl;
	_getch();
	system("CLS");

	//Size of amount of soldiers
	int size;
	cout << "How many soldiers are able to go for help? ";
	cin >> size;

	//soldiers' new node
	node * soldiers = new node[size];
	string name;

	cout << "What are the names of these Soldiers? ";
	//link list of soldiers
	for (int i = 0; i < size; i++)
	{
		cin >> name;
		cout << "Name of Soldier: " << name << endl;
		(soldiers + i)->name = name;
		if (i != size - 1)
		{
			soldiers[i].next = (soldiers + (i + 1));
		}
		else
		{
			soldiers[i].next = (soldiers + 0);
		}
	}
	//Drawing number
	int start = LordCommander(size);

	node * temp = (soldiers + start);
	

	while (temp->next != (soldiers + start))
	{
		cout << temp->name << endl;
		temp = temp->next;
		//cout who gets cloak
		PassingCloak(soldiers, size, start);
	}
	cout << temp->name << endl;
	//cout winner and  delete rest
	
	system("pause");
}